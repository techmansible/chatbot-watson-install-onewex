# Roles
Below are the roles used in this playbook

* chatbot_watson_install_onewex -- Installs the prerequisites and Watson Explorer oneWEX application (CHATBOT) using this playbook.

# chatbot_watson_install_onewex 

* Create the Directory structures required for DOCKER & WATSON installation.
* Mounts the required filesystems.
* Create the soft link in linux for complying the DOCKER & WATSONN specific directory structure.
* Installs & configures Docker and it storage.
* Installs & configures Watson Explorer oneWEX.
